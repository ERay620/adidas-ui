package com.adidas.utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Driver {

    private ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<WebDriver>();
    private String browser;
    private Logger log;
    WebDriver driver = null;

    public Driver(String browser, Logger log) {
        this.browser = browser.toLowerCase();
        this.log = log;
    }

    public WebDriver createDriver() {

        log.info("Create driver: " + browser);              // Create driver
        System.out.println("Create driver: " + browser);

        switch (browser) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driverThreadLocal.set(new ChromeDriver());
                driver = driverThreadLocal.get();
                break;

            case "chromeheadless":
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--headless");
                driverThreadLocal.set(new ChromeDriver(chromeOptions));
                driver = driverThreadLocal.get();
                break;


            default:
                System.out.println("Do not know how to start: " + browser + ", starting chrome.");
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                driverThreadLocal.set(new ChromeDriver());
                driver = driverThreadLocal.get();
                break;
        }

        return driver;
    }

}
