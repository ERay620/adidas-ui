package com.adidas.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {


    @FindBy(id = "nava")
    public WebElement productStore;  // we have 2 dif locator work on
    @FindBy(xpath = "//a[text()  = 'Sony vaio i5']")
    public WebElement sonyVaioI5;

    public By sonyVaioI7 = By.xpath("//*[@id=\"tbodyid\"]/div[2]/div/div/h4/a");
    public By productStore2 = By.id("nava");
    public By monitor = By.xpath("//a[@onclick=\"byCat('monitor')\"]");
    public By notebook = By.xpath("//a[@onclick=\"byCat('notebook')\"]");
    public By phone = By.xpath("//a[@onclick=\"byCat('phone')\"]");
    public By addToChartSonyVaioI5 = By.xpath("//a[@onclick=\"addToCart(8)\"]");
    public By addToChartSonyVaioI7 = By.xpath("//a[@onclick=\"addToCart(9)\"]");
    public By clickChart = By.id("cartur");
    public By deleteItemI7 = By.xpath("(//a[text()  ='Delete'])[2]");
    public By placeHolder = By.xpath("//button [text() = 'Place Order']");

    public String pageUrl = "https://www.demoblaze.com/index.html";

    public HomePage(WebDriver driver, Logger log) {  //So this constructor will receive Web driver, driver and logger log
        super(driver, log);

    }


    /**
     * Open WelcomePage with it's url
     */
    public void openPage() {
        log.info("Opening page: " + pageUrl);
        openUrl(pageUrl);     // driver.grt(pageUrl)
        log.info("Page opened!");
    }

    public boolean isProductStoreDisplayed() {
        log.info("isProductStoreDisplayed");
        return productStore.isDisplayed();
    }

    public void navigateToPhone() {
        click(phone);
        sleep(500);
    }

    public void navigateToNotebook() {
        click(notebook);
        sleep(500);
    }

    public void navigateToMonitor() {
        click(monitor);
    }

    public void addToChartSonyVaioI5() {
        sonyVaioI5.click();
        click(addToChartSonyVaioI5);

    }

    public void clickOnProductStore() {
        click(productStore2);
        sleep(500);

    }

    public void addToChartSonyVaioI7() {
        click(sonyVaioI7);
        click(addToChartSonyVaioI7);

    }

    public void acceptAlert() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        sleep(500);
        alert.accept();
    }

    public void navigateToChart() {
        click(clickChart);
    }

    public void deleteSonyVaioI7() {
        click(deleteItemI7);
    }

    public void clickPlaceHolder() {
        click(placeHolder);
        sleep(500);
        click(placeHolder);
        sleep(500);

    }


}


