package com.adidas.pages;

import com.adidas.utilities.TestUtil;
import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage extends TestUtil {

    public BasePage(WebDriver driver, Logger log) {
        this.driver = driver;
        this.log = log;
        PageFactory.initElements(driver, this);
    }


    /**
     * Open page with given URL
     */
    protected void openUrl(String url) {
        driver.get(url);
    }

    protected WebElement find(By locator) {          //Find element using given locator
        return driver.findElement(locator);
    }

    // Find all elements using given locator
    protected List<WebElement> findAll(By locator) {
        return driver.findElements(locator);
    }

    //Click on element with given locator when its visible
    protected void click(By locator) {    //Click on element with given locator when its visible
        waitForVisibilityOf(locator, 5);
        find(locator).click();
    }

    // Type given text into element with given locator
    protected void type(String text, By locator) {
        waitForVisibilityOf(locator, 5);
        find(locator).sendKeys(text);
    }

    protected void waitForVisibilityOf(By locator, Integer... timeOutInSeconds) {   //  ...  means optional
        int attempts = 0;
        while (attempts < 2) {
            try {
                waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                        (timeOutInSeconds.length > 0 ? timeOutInSeconds[0] : null));
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
    }

    //Wait for specific ExpectedCondition for the given amount of time in seconds
    private void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds) {
        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : 30;
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }

}
