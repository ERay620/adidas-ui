package com.adidas.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class PlaceOrder extends BasePage {

    public By name = By.id("name");
    public By country = By.id("country");
    public By city = By.id("city");
    public By card = By.id("card");
    public By month = By.id("month");
    public By year = By.id("year");
    public By purchaseButton = By.xpath("//button[text() ='Purchase']");
    public By purchaseInfo = By.xpath("//p[@class=\"lead text-muted \"]");


    public PlaceOrder(WebDriver driver, Logger log) {  //So this constructor will receive Web driver, driver and logger log
        super(driver, log);

    }


    public void fillPurchaseForm() {
        type("myName", name);
        type("myCountry", country);
        type("myCity", city);
        type("myCard", card);
        type("myMonth", month);
        type("myYear", year);
    }

    public void completePurchase() {
        click(purchaseButton);
    }

    public void purchaseInfo() {

        String[] partsInfo = driver.findElement(purchaseInfo).getText().split(" ");
        System.out.println("The id number is: " + partsInfo[1].substring(0, partsInfo[1].length() - 7));
        System.out.println("The purchase amount : " + partsInfo[2] + " USD");

        Assert.assertEquals(partsInfo[2],  String.valueOf(790), "Product amount is different than expected");
    }


}
