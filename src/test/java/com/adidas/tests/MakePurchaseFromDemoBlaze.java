package com.adidas.tests;

import com.adidas.pages.HomePage;
import com.adidas.pages.PlaceOrder;
import com.adidas.utilities.TestUtil;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

public class MakePurchaseFromDemoBlaze extends TestUtil {

    @Test(enabled = true)
    public void makePurchaseFromDemoBlaze()  {
        HomePage homePage = new HomePage(driver, log);
        PlaceOrder placeOrder = new PlaceOrder(driver, log);
        test=extent.createTest("TC-1 :Make a purchase from Demo Blaze");

        homePage.openPage();                               test.log(Status.PASS, "- Open Home Page");
        homePage.navigateToNotebook();                     test.log(Status.PASS, "- Navigate to Notebook");
        homePage.addToChartSonyVaioI5();                   test.log(Status.PASS, "- Add VaioI5 to chart");
        homePage.navigateToChart();
        homePage.clickOnProductStore();
        homePage.navigateToNotebook();
        homePage.addToChartSonyVaioI7();                   test.log(Status.PASS, "- Add VaioI7 to chart");
        homePage.acceptAlert();
        homePage.navigateToChart();
        homePage.deleteSonyVaioI7();                       test.log(Status.PASS, "- Delete VaioI7 to chart");
        homePage.clickPlaceHolder();
        placeOrder.fillPurchaseForm();                     test.log(Status.PASS, "- Fill out the purchase form");
        placeOrder.completePurchase();                     test.log(Status.PASS, "- Complete the purchase");
        placeOrder.purchaseInfo();                         test.log(Status.PASS, "- Verify the purchase details");

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        takeScreenshot("Purchase Order Detail");   test.log(Status.PASS, "- Take the screenshot");


    }
}
